'use strict';
const { dest, series, parallel } = require('gulp');
const sass = require('gulp-sass');
const gulp = require('gulp');
const { src } = require('gulp');
sass.compiler = require('node-sass');

function defaultTask(cb) {
  cb();
}

function css(cb) {
  return src('./src/sass/**/*.scss')
            .pipe(sass().on('error', sass.logError))
            .pipe(dest('./dist/css'));
}

function html(cb) {
  return src('./src/html/index.html')
            .pipe(dest('dist'));
}

const ts = require("gulp-typescript");
var tsProject = ts.createProject("tsconfig.json");

gulp.task("default", function() {
  return tsProject.src().pipe(tsProject()).js.pipe(gulp.dest("dist"));
});

exports.default = parallel(css, html);
